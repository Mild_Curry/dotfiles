#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

neofetch # --off #--ascii "/home/brendan/.config/neofetch/ASCII Art/ASCII Cthulhu"

(cat $HOME/.config/wpg/sequences &)

alias config='/usr/bin/git --git-dir=$HOME/gitlab/dotfiles/ --work-tree=$HOME'
alias configwall='/usr/bin/git --git-dir=$HOME/gitlab/wallpapers/ --work-tree=$HOME'
